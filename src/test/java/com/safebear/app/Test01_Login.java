package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by cca_student on 19/07/2018.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void test01login () {
        //Step 1 Confirm we're on the Welcome page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 Click on the Login link and the Login Page loads
        welcomePage.clickOnlogin();
        //Step 3 Confirm that we're now on the Login page
        assertTrue(loginPage.checkCorrectPage());
        //Step 4 login with valid credentials
        loginPage.login("testuser", "testing");
        //Step 5 Check that we're now on the User Page
        assertTrue(userPage.checkCorrectPage());

    }
}
